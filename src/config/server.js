const express = require('express');
const routes = require('../routes/routes');
const dotenv = require('dotenv/config');
const body = require('body-parser')
const cors = require('cors')

const app = express();

app.use(cors())
app.use(body.json());
app.use(routes);

app.listen(3003, () =>{
  console.log("Listen on 3003")
})