const axios = require("axios");

exports.getAll = async (req, res) => {
  try {
    const products = await axios.get(`${process.env.API_DATALAKE}/products`);

    res.status(200).json(products.data);
    
  } catch (err) {
    res.status(400).json({
      message: err
    });
  }
}

exports.getById = async (req, res) =>{
  try{
    const { id } = req.params;

    const productsId = await axios.get(`${process.env.API_DATALAKE}/products/${id}`)

    res.status(200).json(productsId.data);
  }catch(err){
    res.status(400).json({
      message: err
    })
  }
}