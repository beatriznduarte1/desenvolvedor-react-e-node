const axios = require("axios");

const getPost = require("./products.controller");

exports.createUser = async(req, res) =>{
  try{
      const { name, email, product_id, password } = req.body;

      const createUser = await axios.post(`${process.env.API_DATALAKE}/user/`, {
        name,
        email,
        product_id,
        password
      })

      res.status(200).json(createUser.data);

  }catch(err){
    res.status(400).json({
      message: err
    });
  }
}

exports.updateUser = async(req, res) =>{
  try{
    const { id } = req.params; 

    const { name, email, password, product_id } = req.body;

    const updateUsers = await axios.put(`${process.env.API_DATALAKE}/user/${id}`,{
      name,
      email,
      password,
      product_id
    })

    res.status(200).json(updateUsers.data)

  }catch(err){
    res.status(400).json({
      message: err
    });
  }
}

exports.getUserById = async(req, res) =>{
  try{
    const { id } = req.params;

    const userID = await axios.get(`${process.env.API_DATALAKE}/user/${id}`)

    res.status(200).json(userID.data);

  }catch(err){
    res.status(400).json({
      message: err
    });
  }
}

exports.loginUser = async(req,res) =>{
  try{
    const { email, password } = req.query;

    const users = await axios.get(`${process.env.API_DATALAKE}/user/`)

    let userInfo = users.data.filter(user => user.email === email && user.password === password);

    res.status(200).json(userInfo);

  }catch(err){
    res.status(400).json({
      message: err
    });
  }
}

exports.getUserPost = async(req, res) =>{
  try{
    const { id } = req.params;

    const user = await axios.get(`${process.env.API_DATALAKE}/user/${id}`)

    const getPost = await axios.get(`${process.env.API_BLOG}/posts/`)

    let userPost = getPost.data.filter(posts => posts.products.includes(user.data.product_id));

    res.status(200).json(userPost);

  }catch(err){
    res.status(400).json({
      message: err
    });
  }

}

exports.productsPosts = async(req, res) => {
  try{
    const { idPost } = req.params;

    const productPost = await axios.get(`${process.env.API_BLOG}/posts/${idPost}`)

    res.status(200).json(productPost.data);

  }catch(err){

    res.status(400).json({
      message: err
    });

  }
}