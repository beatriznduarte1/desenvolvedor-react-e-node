const express = require('express')

const routes = express.Router();

const ProductsController = require("../controllers/products.controller");
const UsersController = require("../controllers/users.controller");


routes.get("/api/v1/products", ProductsController.getAll)
routes.get("/api/v1/products/:id", ProductsController.getById)

routes.post("/api/v1/user/", UsersController.createUser)
routes.put("/api/v1/user/:id", UsersController.updateUser)

routes.get("/api/v1/user/", UsersController.loginUser)
routes.get("/api/v1/user/:id", UsersController.getUserById)
routes.get("/api/v1/user/:id/posts", UsersController.getUserPost)
routes.get("/api/v1/user/:idUser/posts/:idPost", UsersController.productsPosts)




module.exports = routes;