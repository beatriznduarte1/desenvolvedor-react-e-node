# Eleven App
Olá, para você acessar o App é necessário seguir alguns procedimentos antes.
    
## Clonando o projeto
Baixe o repositório do App na sua máquina: 

```sh
$ git clone https://gitlab.com/beatriznduarte1/desenvolvedor-react-e-node.git
```

Acesse o diretório e em seguida instale todas as dependências:

```sh
$ npm install
```

Acesse o diretório /frontend e rode o seguinte comando:

```sh
$ yarn install
```

Após a instalação, volte para o diretório anterior e instale o json-server: 

```sh
$ npm install -g json-server
```

## APIs

Para executar as apis, execute os seguintes comandos em terminais distintos: 

```sh
# Serviço de blog
$ npm run api_blog

# Serviço de datalake
$ npm run api_datalake

# Api do backend
$ npm start
```

## Executando o Front

Para executar o front, em um terminal distinto, execute o seguinte comando: 

```sh
$ yarn start
```

## Observações 

Para acessar a aplicação é necessário realizar um cadastro antes. 

Caso queira alterar informações do cadastro, o acesso é através do nome do usuário. 
