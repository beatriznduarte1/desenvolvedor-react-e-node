import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

const Header = (props) => {

  function handleLogout(){
    localStorage.clear();
  }

    return (
        <>
          <header>
            <h1><Link to="/" onClick={handleLogout}>Eleven App</Link></h1>
          {props.name ? <p>Olá, <Link to={`/profile/${props.id}`}>{props.name}</Link></p> : <Link to="/" className="button-login">Login</Link>}
          </header>
        </>
    );
}

export default Header;