import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import moment from 'moment';

import Header from '../Header'
import api from '../../services/api'

import FeedImg  from '../../assets/financeImg.jpg'

import "./style.css";

const Feed = ({match}) =>{

  const id = localStorage.getItem('userId')
  const nameUser = localStorage.getItem('userName')

  const [userPost, setUserPost] = useState({})
  const [search, setSearch] = useState('')
  const [searchResult, setSearchResult] = useState([])

  useEffect(() =>{
    api.get(`/user/${id}/posts`).then(response =>{
      setUserPost(response.data)
    })
  }, [id])


  useEffect(() =>{
    if(userPost.length > 0){
      const postSearch = userPost.filter(post => post.title.includes(search));

      setSearchResult(postSearch)

    }
    
  },[search, userPost])

  return (
    <>
      <Header name={nameUser} id={id} />
      <div className="container-feed">
        <div className="header-feed">
          <h2>Feed</h2>
          <form>
            <input type="text" placeholder="Search" onChange={(e) => setSearch(e.target.value)}/>
          </form>
        </div>
        <div className="cards-feed">
          {userPost.length > 0 ? 
          <ul>
            {searchResult.map((posts) => (
              <li key={posts.id}>
                <Link to={`/post/${posts.id}`}>
                <img
                  src={FeedImg}
                  alt="Duas pessoas usando o notebook e escrevendo no papel"
                />
                <h3>{posts.title}</h3>
                <span dangerouslySetInnerHTML={{ __html: posts.content.substr(0, 80) }}></span>
                <span>{moment(posts.date,"YYYYMMDD").fromNow()}</span>
                </Link>
              </li>
            ))}
          </ul>
          : 
          <p>Ainda não existe posts para esse produto</p>
          }
          {userPost.length > 0 && searchResult.length === 0 &&
            <p>Não encontramos nenhum resultado para essa busca</p>
          }
        </div>
      </div>
    </>
  );
}

export default Feed;