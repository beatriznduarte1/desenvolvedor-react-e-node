import React,{ useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

import Header from '../Header'
import api from '../../services/api'

import FeedImg  from '../../assets/financeImg.jpg'
import Arrow from '../../assets/return.svg'

import './style.css'

const Post = ({match}) =>{
    const [post, setPost] = useState([])

    const id = localStorage.getItem('userId')
    const nameUser = localStorage.getItem('userName')
    const { id:postId } = match.params

    useEffect(() =>{
        api.get(`/user/${id}/posts/${postId}`).then(response =>{
          setPost(response.data)
        })
    },[postId, id])
    
  return (
    <>
      <Header name={nameUser} id={id} />
      <div className="container-post">
        <Link to={`/feed/${id}`}><img src={Arrow} alt="voltar" /></Link>
        <div className="post-header">
          <h2>{post.title}</h2>
          <p>{post.author}</p>
        </div>
        <div className="post-content">
          <div className="post-img">
            <img
              src={FeedImg}
              alt={"Duas pessoas usando o notebook e escrevendo no papel"}
            />
          </div>
          <span dangerouslySetInnerHTML={{ __html: post.content }}></span>
        </div>
      </div>
    </>
  );
}

export default Post;