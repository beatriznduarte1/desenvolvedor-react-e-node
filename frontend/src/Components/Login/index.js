import React, {useState, useEffect} from 'react'
import { Link,useHistory } from 'react-router-dom'
import Header from '../Header'
import Swal from 'sweetalert2';

import api from '../../services/api'


import './style.css'

const Login = () =>{

  const id = localStorage.getItem('userId')
  const nameUser = localStorage.getItem('userName')

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const history = useHistory();

  useEffect(() =>{
    if (id && nameUser){
      history.push(`/feed/${id}`)
    }
  },[id,nameUser,history])

  async function handleLogin(e){
    e.preventDefault();

    try{
      const response = await api.get(`/user/?email=${email}&password=${password}`)

      if(response){
        localStorage.setItem('userId', response.data[0].id)
        localStorage.setItem('userName', response.data[0].name)

        history.push(`/feed/${response.data[0].id}`)
      }

    }catch(err){
      Swal.fire({
        text: "Email ou senha incorreto, tente novamente",
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  }

  return (
    <>
      <Header />
      <div className="container container-login">
        <h2>Acessar</h2>
        <form onSubmit={handleLogin}>
          <input 
            type="email" 
            placeholder="Email" 
            value={email}
            onChange={e => setEmail(e.target.value)}/>
          <input 
            type="password" 
            placeholder="Senha" 
            onChange={e => setPassword(e.target.value)}/>
          <input type="submit" value="Entrar" />
          <p>Esqueci a senha</p>
          <Link to="/register">Registrar</Link>
        </form>
      </div>
    </>
  );
}

export default Login;