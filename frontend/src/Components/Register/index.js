import React, {useEffect, useState} from 'react';
import Swal from 'sweetalert2';

import Header from '../Header'
import api from '../../services/api'

import './style.css';

const Register = () => {

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  let [product_id, setProduct_id] = useState()
  const [password, setPassword] = useState('')
  const [products, setProducts] = useState([])

  useEffect(()=>{
      api.get('/products').then(response =>{
        setProducts(response.data)
      })
  },[])

  async function handleRegister(e){
    e.preventDefault();

    product_id = parseInt(product_id)

    const data = {
      name,
      email,
      product_id,
      password
    }

    try{
      const response = await api.post('/user/', data)

      if(response){
        Swal.fire({
          title: 'Sucesso',
          text: 'Cadastro realizado',
          icon: 'success',
          confirmButtonText: '<a href="/" style="text-decoration: none; color: #FFF;">OK</a>',
        })
      }

    }catch(err){
      Swal.fire({
        text: "Erro ao realizar o cadastro, tente novamente mais tarde",
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  }

    return (
        <>
          <Header />
          <div className="container">
            <div>
              <h2>Registro</h2>
            </div>
            <form onSubmit={handleRegister}>
              <input 
                type="text" 
                value={name}
                onChange={e => setName(e.target.value)} 
                placeholder="Nome"/>
              <input 
                type="email" 
                value={email}
                onChange={e => setEmail(e.target.value)} 
                placeholder="Email"/>
              <select onClick={e => setProduct_id(e.target.value)} defaultValue="">
                <option disabled value="">Produtos</option>
              {products.map(product => (
                  <option key={product.id} value={product.id}>{product.name}</option>
              ))}
              </select>
              <input 
                type="password" 
                value={password}
                onChange={e => setPassword(e.target.value)}
                placeholder="Senha"/>
              <input type="submit" value="Registrar" />
            </form>
          </div>
        </>
    );
}

export default Register;