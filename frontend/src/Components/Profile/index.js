import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import Header from '../Header'
import api from '../../services/api';

import profileImg from '../../assets/userImg.png'

import './style.css'

const Profile = () =>{

  const [products, setProducts] = useState([])
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  let [product_id, setProduct_id] = useState('')

  const id = localStorage.getItem('userId')
  const nameUser = localStorage.getItem('userName')

useEffect(() => {
  api.get("/products").then((response) => {
    setProducts(response.data);
  });

  api.get(`/user/${id}`).then((response) =>{
    setName(response.data.name)
    setEmail(response.data.email)
    setPassword(response.data.password)
    setProduct_id(response.data.product_id)
  })
}, [id]);

  async function handleUpdateUser(e){
    e.preventDefault();

    product_id = parseInt(product_id)

    const data = {
      name,
      email,
      password,
      product_id
    }

    try{
      const response = await api.put(`/user/${id}`, data)

      if (response) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Seus dados foram atualizados',
          icon: 'success',
          confirmButtonText: 'OK',
        })
      }
    }catch(err){
      Swal.fire({
        text: "Erro ao atualizar cadastro, tente novamente mais tarde",
        icon: "error",
        confirmButtonText: "OK",
      });

    }
  }


  return (
    <>
      <Header name={nameUser} id={id} />
      <div className="container-profile">
        <h2>Perfil</h2>
        <div className="profile-content">
          <div className="userImg">
            <img src={profileImg} alt="Gato olhando para a camera" />
          </div>
          <div className="form-content">
            <h3>{name}</h3>
            <form onSubmit={handleUpdateUser}>
              <input 
                type="email" 
                placeholder="Email" 
                onChange={e => setEmail(e.target.value)}
                value={email}/>
              <select value={product_id} onChange={e => setProduct_id(e.target.value)}>
                <option disabled value="">Produtos</option>
                {products.map(product => (
                  <option key={product.id} value={product.id}>{product.name}</option>
                ))}
              </select>
              <div className="profile-button">
                <input type="submit" value="Salvar" />
                <Link to={`/feed/${id}`}>Sair</Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;