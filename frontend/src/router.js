import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Register from './Components/Register'
import Login from './Components/Login'
import Feed from './Components/Feed'
import Post from './Components/Post'
import Profile from './Components/Profile'

const Routes = () => {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/feed/:id" component={Feed} />
          <Route path="/post/:id" component={Post} />
          <Route path="/profile/:id" component={Profile} />
        </Switch>
      </BrowserRouter>
    );
}

export default Routes;